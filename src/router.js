import Vue from 'vue'
import Router from 'vue-router'

const ComponentA = () => import('./components/ComponentA.vue')
const ComponentB = () => import('./components/ComponentB.vue')
const Demo = () => import('./components/Child.vue')
Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/demo',
      component: Demo
    },
    {
      path: '/b',
      component: ComponentB
    }
  ]
})

export default router