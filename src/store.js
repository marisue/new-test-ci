import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const moduleA = {
  actions: {},
  mutations: {},
  state: {}
}

const moduleB = {
  actions: {},
  mutations: {},
  state: {}
}
                      
export default new Vuex.Store({
  modules: {
    moduleA,
    moduleB
  }     
})